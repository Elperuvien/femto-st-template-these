# Template de rédaction de thèse

Ces fichiers sont fournis afin de faciliter (autant que possible) la mise en
place d'une structure pour la rédaction d'une thèse.

Ce template est associé aux packages pour la compilation de la thèse disponible
dans [ce dépot](https://gitlab.com/Elperuvien/packages-theses-femto-ubfc)

## Utilisation du template

- Copier l'ensemble du dépot sur votre machine. Après installation des packages
  associés, il est possible de compiler directement le document
  `ma_these_doctorat.tex`. C'est un bon test pour savoir si tout est bien
  installé.

- Renommer les fichiers utiles et rédiger !!!
	- Le titre, le jury de la couverture sont définis dans 
	`./titre_page_de_garde/definition_titre.tex`
	- La quatrième de couverture et les chemins vers les résumés français
	et anglais sont définis dans 
    `./titre_page_de_garde/definition_quatrieme_couv.tex` 

## Mise en garde

- Il est recommandé d'utiliser latexmk pour une compilation automatique de la
  thèse. Le fichier `compilationThesseLatexmk` donne un exemple de commande
  latexmk, utilisé dans un environement linux

- La structure est indicative, elle permet de bien séparer chaque fichier pour
  faciliter le suivi des versions, et éviter une corruption de toute la thèse
  en cas de problème

- **ATTENTION : si vous cloner le dépot, il ne faut pas commiter votre thèse,
  il est préférable de copier ce template dans votre répertoire**

- Le template est distribué sans aucune garantie.
